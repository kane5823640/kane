// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#include "nc-frame.h"
#include "nc-platform.h"

#include <stdlib.h>

typedef struct NCFrame {
    Frame base;
    WINDOW *frm_wnd;
} NCFrame;

static void NCFrm_Pfrm_destroy(Frame *base) {

    NCFrame *self = (NCFrame *)base;

    wborder(self->frm_wnd, ' ', ' ', ' ',' ',' ',' ',' ',' ');
    wrefresh(self->frm_wnd);
    delwin(self->frm_wnd);

    free(base->cells);
    free(base);
}

NCFrame *NCFrm_create(FrmRect rect) {

    NCFrame *frm = malloc(sizeof(*frm));

    frm->base.rect = rect;
    frm->base.cells = malloc(rect.size.rows *
                             rect.size.columns *
                             sizeof(*frm->base.cells));
    frm->base.destroy = NCFrm_Pfrm_destroy;

    frm->frm_wnd = newwin(rect.size.rows,
                          rect.size.columns,
                          rect.coords.y,
                          rect.coords.x);
    Log_debug("Window pointer: %p\n", frm->frm_wnd);
    wrefresh(frm->frm_wnd);
    return frm;
}

void NCFrm_repaint(NCFrame *self) {
    FrmCell *cell = self->base.cells;
    for (int row = 0; row != self->base.rect.size.rows; ++row) {
        wmove(self->frm_wnd, row, 0);
        for (int column = 0; column != self->base.rect.size.columns; ++column) {
            waddch(self->frm_wnd, cell->chr);
            ++cell;
        }
    }
    wrefresh(self->frm_wnd);
}
