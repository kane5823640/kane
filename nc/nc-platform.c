// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#include "nc-platform.h"
#include "nc-window.h"

#include <curses.h>

#include <stdbool.h>
#include <stdlib.h>

static bool NC_initialized = false;

typedef struct NCPlatform {
    Platform base;
} NCPlatform;

static ErrorCode NCPfrm_open_window(Platform *base, Window **wnd_base) {
    (void)base;
    return NCWnd_create(wnd_base);
}

static void NCPfrm_destroy(Platform *base) {
    if (NC_initialized) {
        endwin();
        NC_initialized = false;
    }
    free(base);
}

ErrorCode NCPfrm_init(Platform **ret) {

    ErrorCode err;

    if (NC_initialized) {
        err = ERR_NC_ALREADY_INITIALIZED;
        goto already_init;
    }

    (void)initscr();

    raw();
    noecho();
    keypad(stdscr, TRUE);
    refresh();

    NC_initialized = true;

    NCPlatform *self = malloc(sizeof(*self));

    Platform *base = (Platform *)self;
    base->open_window = NCPfrm_open_window;
    base->destroy = NCPfrm_destroy;

    *ret = base;
    return ERR_NONE;

already_init:
    *ret = NULL;
    return err;
}
