// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#include "nc-window.h"

#include <curses.h>

#include <stdbool.h>

typedef struct NCWindow {
    Window base;
    Layout layout;
} NCWindow;

static NCWindow NCWnd;
static bool NC_window_initialized = false;

static Layout *NCWindow_Pfrm_reset_layout(Window *base) {
    NCWindow *self = (NCWindow *)base;
    Layout *ret = &self->layout;

    if (ret->root)
        LNd_destroy(ret->root);

    int rows, columns;
    getmaxyx(stdscr, rows, columns);

    FrmRect wnd_rect ={ { 0, 0 }, { rows, columns } };
    ret->root = LNd_create_frame((Frame *)NCFrm_create(wnd_rect));
    ret->focus = NULL;

    return ret;
}

static Key NCWindow_Pfrm_await_input(Window *base) {
    (void)base;
    return getch();
}

static void NCWindow_Pfrm_repaint(Window *base) {
    NCWindow *self = (NCWindow *)base;
    LayoutNode *n;
    for (n = LNd_first_frame(self->layout.root); n; n = LNd_next_frame(n)) {
        Frame *frm_base = LNd_peek_frame(n);
        NCFrm_repaint((NCFrame *)frm_base);
    }
}

static void NCWindow_Pfrm_close(Window *base) {
    NCWindow *self = (NCWindow *)base;
    if (NC_window_initialized) {
        if (self->layout.root)
            LNd_destroy(self->layout.root);
        NC_window_initialized = false;
    }
}

ErrorCode NCWnd_create(Window **wnd_base) {
    if (NC_window_initialized) {
        *wnd_base = NULL;
        return ERR_NC_ALREADY_INITIALIZED;
    }

    NCWnd.base.reset_layout = NCWindow_Pfrm_reset_layout;
    NCWnd.base.await_input = NCWindow_Pfrm_await_input;
    NCWnd.base.repaint = NCWindow_Pfrm_repaint;
    NCWnd.base.close = NCWindow_Pfrm_close;

    NCWnd.layout.root = NULL;
    NCWnd.layout.focus = NULL;

    NC_window_initialized = true;

    *wnd_base = (Window *)&NCWnd;
    return ERR_NONE;
}
