// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#include "shared.h"
#include "sdl/sdl-platform.h"
#include "nc/nc-platform.h"
#include "cli/client.h"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {

    ErrorCode err;
    Platform *pfrm;

    Log_debug("Application starting...\n");

    for (int i = 0; i != argc; ++i) {
        if (strcmp("-nw", argv[i]) == 0) {
            Log_debug("Explicit request for terminal interface.\n");
            goto term_mode;
        }
    }

    if ((err = SDLPfrm_init(&pfrm)) != ERR_NONE) {
        Log_debug("Gui initialization failed with error: %s.\n"
                  "Fall back to terminal.\n",
                  ErrorString(err));
        err = ERR_NONE;
        goto term_mode;
    }

    if ((err = Cli_run(pfrm)) != ERR_NONE)
        goto failed_run;
    else
        goto succeeded;

term_mode:
    if ((err = NCPfrm_init(&pfrm)) != ERR_NONE)
        goto failed_init;

    if ((err = Cli_run(pfrm)) != ERR_NONE)
        goto failed_run;

succeeded:
    pfrm->destroy(pfrm);
    Log_debug("Exiting with SUCCESS status.\n");
    return 0;

failed_run:
    pfrm->destroy(pfrm);

failed_init:
    Log_debug("Exiting with error status...\n"
              "Error: [%s] -_-\n",
              ErrorString(err));
    return 1;
}
