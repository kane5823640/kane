// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef FRAME_H
#define FRAME_H

#include "../shared.h"

typedef struct FrmCoords {
    int x, y;
} FrmCoords;

typedef struct FrmSize {
    int rows;
    int columns;
} FrmSize;

typedef struct FrmRect {
    FrmCoords coords;
    FrmSize size;
} FrmRect;

typedef struct FrmCell {
    Char chr;
} FrmCell;

typedef struct Frame {
    FrmRect rect;
    FrmCell *cells;
    void (*destroy)(struct Frame *self);
} Frame;

void Frm_clear(Frame *frm, FrmCell *clear_value);
void Frm_test_fill(Frame *rfm);

#endif
