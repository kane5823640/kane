// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LAYOUT_H
#define LAYOUT_H

#include "frame.h"

#include <stddef.h>

typedef struct LayoutNode LayoutNode;

typedef enum LayoutNodeType {
    LNODE_FRAME,
    LNODE_VSPLIT,
    LNODE_HSPLIT
} LayoutNodeType;

typedef struct LayoutSplit {
    FrmRect rect;
    struct {
        LayoutNode **data;
        size_t size, cap;
    } children;
} LayoutSplit;

typedef struct LayoutNode {
    union {
        Frame *frame;
        LayoutSplit split;
    };
    struct LayoutNode *parent;
    LayoutNodeType type;
} LayoutNode;

LayoutNode *LNd_create_frame(Frame *frm);
void LNd_destroy(LayoutNode *n);

LayoutNode *LNd_next_node(LayoutNode *n);
LayoutNode *LNd_first_frame(LayoutNode *n);
LayoutNode *LNd_next_frame(LayoutNode *n);
Frame *LNd_peek_frame(LayoutNode *n);

typedef struct Layout {
    LayoutNode *root;
    LayoutNode *focus;
} Layout;

#endif
