// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#include "frame.h"

#include <stddef.h>
#include <stdio.h>
#include <unistd.h>

void Frm_clear(Frame *frm, FrmCell *clear_value) {
    for (int i = 0; i != frm->rect.size.rows * frm->rect.size.columns; ++i) {
        frm->cells[i] = *clear_value;
    }
}

void Frm_test_fill(Frame *frm) {
    FILE *file = fopen("./kane.c", "r");
    char *line = NULL;
    ssize_t line_len;
    size_t line_cap;
    int row = 0;
    while ((line_len = getline(&line, &line_cap, file)) != -1) {
        if (row == frm->rect.size.rows)
            break;
        char *rd = line;
        FrmCell *wrt = frm->cells + row * frm->rect.size.columns;
        int column = 0;
        while (column != frm->rect.size.columns &&
               *rd &&
               *rd != '\n' &&
               *rd != '\t') {
            wrt->chr = *rd;
            ++wrt;
            ++rd;
        }
        ++row;
    }
}
