// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#include "layout.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

static bool LNd_next_up(LayoutNode **prev, LayoutNode **curr) {

    LayoutNode *p = *prev;
    LayoutNode *c = *curr;

    if (!c) return false;

    switch (c->type) {
    case LNODE_FRAME:
        fprintf(stderr, "Impossible state [%s:%d].\n", __FILE__, __LINE__);
        exit(1);
        break;

    case LNODE_VSPLIT:
    case LNODE_HSPLIT:
        if (p == c->split.children.data[c->split.children.size - 1]) {
            *prev = *curr;
            *curr = (*curr)->parent;
            return true;
        }
        break;
    }

    return false;
}

LayoutNode *LNd_create_frame(Frame *frm) {
    LayoutNode *ret = malloc(sizeof(*ret));
    ret->frame = frm;
    ret->parent = NULL;
    ret->type = LNODE_FRAME;
    return ret;
}

void LNd_destroy(LayoutNode *n) {
    switch (n->type) {
    case LNODE_VSPLIT:
    case LNODE_HSPLIT:
        for (size_t i = 0; i != n->split.children.size; ++i) {
            LayoutNode *child = n->split.children.data[i];
            LNd_destroy(child);
        }
        free(n->split.children.data);
        break;

    case LNODE_FRAME:
        n->frame->destroy(n->frame);
        break;
    }
    free(n);
}

LayoutNode *LNd_next_node(LayoutNode *n) {

    LayoutNode *prev = n;
    LayoutNode *curr = n->parent;

    switch (n->type) {
    case LNODE_FRAME:
        while (curr && LNd_next_up(&prev, &curr)); // NOP
        if (!curr)
            return NULL;
        for (size_t i = 0; i != curr->split.children.size - 1; ++i)
            if (prev == curr->split.children.data[i])
                return curr->split.children.data[i + 1];
        break;

    case LNODE_VSPLIT:
    case LNODE_HSPLIT:
        return n->split.children.data[0];
    }

    fprintf(stderr, "Imposible state [%s:%d].\n", __FILE__, __LINE__);
    exit(1);
}

LayoutNode *LNd_first_frame(LayoutNode *n) {
    if (LNd_peek_frame(n))
        return n;
    else
        return LNd_next_frame(n);
}

LayoutNode *LNd_next_frame(LayoutNode *n) {
    for (n = LNd_next_node(n); n; n = LNd_next_node(n)) {
        if (LNd_peek_frame(n)) break;
    }
    return n;
}

Frame *LNd_peek_frame(LayoutNode *n) {
    if (n && n->type == LNODE_FRAME)
        return n->frame;
    else
        return NULL;
}
