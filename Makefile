# Copyright (C) 2020 Krzysztof Stachowiak
#
# This file is part of the Language Programming Language compiler.
#
# Language Programming Language is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Language Programming Language compiler is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with program.  If not, see <http://www.gnu.org/licenses/>.

LDFLAGS = -std=c11 -lrt -lm `pkg-config --libs sdl2 SDL2_image SDL2_ttf glew opengl freetype2 ncurses`
CFLAGS := -Wall -Wextra -std=c11 -MMD `pkg-config --cflags sdl2 SDL2_image SDL2_ttf glew opengl freetype2 ncurses`

SANITIZE := -fsanitize=address -fsanitize=leak -fsanitize=undefined -static-libasan


REL-CFLAGS := $(CFLAGS) -O2
DBG-CFLAGS := $(CFLAGS) -O0 -g3
SAN-CFLAGS := $(DBG-CFLAGS) $(SANITIZE)


COMMON-SOURCES := cli/client.c \
                  frm/frame.c frm/layout.c \
                  nc/nc-frame.c nc/nc-platform.c nc/nc-window.c \
                  sdl/sdl-frame.c sdl/sdl-platform.c sdl/sdl-render.c sdl/sdl-window.c \
                  shared.c

COMMON-OBJECTS := $(COMMON-SOURCES:%.c=%.o)
COMMON-DBG-OBJECTS := $(COMMON-SOURCES:%.c=%d.o)
COMMON-SAN-OBJECTS := $(COMMON-SOURCES:%.c=%s.o)
COMMON-DEPS := $(COMMON-SOURCES:%.c=%.d)
COMMON-DBG-DEPS := $(COMMON-SOURCES:%.c=%d.d)
COMMON-SAN-DEPS := $(COMMON-SOURCES:%.c=%s.d)

ALL-DEPS := $(COMMON-DEPS) $(COMMON-DBG-DEPS) $(COMMON-SAN-DEPS) kane.d kaned.d kanes.d
ALL-OBJECTS := $(COMMON-OBJECTS) $(COMMON-DBG-OBJECTS) $(COMMON-SAN-OBJECTS) kane.o kaned.o kanes.o


default: kaned kanes
all: kane kaned kanes


kane: kane.o $(COMMON-OBJECTS)
	$(CC) $^ -o $@ $(LDFLAGS)
kaned: kane.o $(COMMON-DBG-OBJECTS)
	$(CC) $^ -o $@ $(LDFLAGS)
kanes: kane.o $(COMMON-SAN-OBJECTS)
	$(CC) $^ -o $@ $(LDFLAGS) $(SAN-CFLAGS)


%.o: %.c
	$(CC) $(REL-CFLAGS) -c $< -o $@
%d.o: %.c
	$(CC) $(DBG-CFLAGS) -c $< -o $@
%s.o: %.c
	$(CC) $(SAN-CFLAGS) -c $< -o $@


.PHONY: clean

clean:
	rm -rf $(ALL-OBJECTS) $(ALL-DEPS) kane kaned kanes

-include $(ALL-DEPS)
