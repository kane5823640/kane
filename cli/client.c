// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#include "client.h"

#include <assert.h>
#include <stdbool.h>

static Layout *wnd_layout;

int Cli_run(Platform *pfrm) {

    ErrorCode err;

    Window *wnd;
    if ((err = pfrm->open_window(pfrm, &wnd))) {
        Log_debug("Client encountered error: %s.\n", ErrorString(err));
        return 1;
    }

    wnd_layout = wnd->reset_layout(wnd);

    Frame *frm = wnd_layout->root->frame;
    FrmCell empty = { .chr = ' ' };
    Frm_clear(frm, &empty);
    Frm_test_fill(frm);
    wnd->repaint(wnd);

    while (true) {
        int key = wnd->await_input(wnd);
        wnd->repaint(wnd);
        if (key == 'q')
            break;
    }

    wnd->close(wnd);
    return 0;
}
