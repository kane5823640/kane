// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#include "shared.h"

#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>

const char *ErrorString(ErrorCode code) {
    switch (code) {
    case ERR_NONE: return "No error";
    case ERR_SDL_FAILED_SETUP_BASICS: return "Failed basic SDL setup";
    case ERR_SDL_FAILED_CREATE_WINDOW: return "Failed creating SDL window";
    case ERR_SDL_FAILED_CREATE_GL_CONTEXT: return "Failed initializing SDL OpenGL context";
    case ERR_SDL_FAILED_CONFIGURE_OPENGL: return "Failed initializing configuring OpenGL";
    case ERR_NC_ALREADY_INITIALIZED: return "NCurses platform initialized twice";
    case ERR_GL_FAILED_COMPILING_SHADER: return "Failed compiling shader";
    case ERR_GL_FAILED_LINKING_PROGRAM: return "Failed linking GPU program";
    case ERR_FT2_FAILED_INIT: return "Failed initializing FreeType2";
    case ERR_FT2_FAILED_FONT_LOAD: return "FreeType2 failed to load font from file";
    case ERR_FT2_FAILED_FONT_RESIZE: return "FreeType2 failed to set size to loaded font";
    case ERR_FT2_FAILED_CHAR_LOAD: return "FreeType2 failed to load/render a character";
    default: return "<Unknown error code>";
    }
}

void Log_debug(const char *format, ...) {
    static bool first_time = true;
    FILE *file;
    if (first_time) {
        file = fopen("debug.log", "w");
        first_time = false;
    } else {
        file = fopen("debug.log", "a");
    }

    va_list ap;
    va_start(ap, format);
    vfprintf(file, format, ap);
    va_end(ap);

    fclose(file);
}
