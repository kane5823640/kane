// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SHARED_H
#define SHARED_H

#include <inttypes.h>

#define ARRR_SIZE(Arrr_) (sizeof(Arrr_) / sizeof(*Arrr_))

#define BIT(N_) (0x1LL << (N_))

typedef enum ErrorCode {
    ERR_NONE                         = 0,
    ERR_FAILED_TO_RUN_ANY_PLATFORM   = BIT( 0),
    ERR_SDL_FAILED_SETUP_BASICS      = BIT( 1),
    ERR_SDL_FAILED_CREATE_WINDOW     = BIT( 2),
    ERR_SDL_FAILED_CREATE_GL_CONTEXT = BIT( 3),
    ERR_SDL_FAILED_CONFIGURE_OPENGL  = BIT( 4),
    ERR_NC_ALREADY_INITIALIZED       = BIT( 5),
    ERR_GL_FAILED_COMPILING_SHADER   = BIT( 6),
    ERR_GL_FAILED_LINKING_PROGRAM    = BIT( 7),
    ERR_FT2_FAILED_INIT              = BIT( 8),
    ERR_FT2_FAILED_FONT_LOAD         = BIT( 9),
    ERR_FT2_FAILED_FONT_RESIZE       = BIT(10),
    ERR_FT2_FAILED_CHAR_LOAD         = BIT(11),
} ErrorCode;

const char *ErrorString(ErrorCode code);

void Log_debug(const char *format, ...);

typedef uint32_t Char;
typedef uint64_t Key;

#endif
