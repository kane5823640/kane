// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RENDER_H
#define RENDER_H

#include <stdint.h>
#include <stddef.h>

typedef struct TexCoords {
    int x;
    int y;
} TexCoords;

typedef struct TexSize {
    int w;
    int h;
} TexSize;

typedef struct TexRect {
    TexCoords coords;
    TexSize size;
} TexRect;

typedef struct RndCharInfo {
    TexRect texrec;
    int bearx;
    int beary;
    long adv;
} RndCharInfo;

typedef struct RndFont {
    void *tex;
    void *data;
    RndCharInfo *(*ascii)(void *data, char chr);
    RndCharInfo *(*utf8)(void *data, uint8_t *chrcode, size_t chrlen);
} RndFontInfo;

#endif
