// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#include "sdl-frame.h"

#include <stdlib.h>

typedef struct SDLFrame {
    Frame base;
} SDLFrame;

static void SDLFrm_Pfrm_destroy(Frame *base) {
    free(base->cells);
    free(base);
}

SDLFrame *SDLFrm_create(int rows, int columns) {
    SDLFrame *frm = malloc(sizeof(*frm));
    frm->base.rect = (FrmRect){ { 0, 0 }, { rows, columns }};
    frm->base.cells = malloc(frm->base.rect.size.rows *
                             frm->base.rect.size.columns *
                             sizeof(*frm->base.cells));
    frm->base.destroy = SDLFrm_Pfrm_destroy;
    return frm;
}
