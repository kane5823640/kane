// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#include "sdl-render.h"

#include <GL/glew.h>
#include <GL/glu.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct GLProgram {
    GLuint id;
} GLProgram;

typedef struct GLCharInfo {
    TexRect texture_rect;
    int bearing_x;
    int bearing_y;
    long advance;
} GLCharInfo;

typedef struct GLFont {
    GLuint texture;
    GLCharInfo char_info[128];
} GLFont;

typedef struct SDLRenderer {
    FT_Library FT_ctx;
    SDL_GLContext glctx;
    GLProgram font_program;
    GLFont default_font;
} SDLRenderer;

static char *GL_Shader_info_log(GLuint shader_id) {
    GLint exp_len;
    glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &exp_len);
    char *ret = malloc(exp_len);
    GLint act_len;
    glGetShaderInfoLog(shader_id, exp_len, &act_len, ret);
    return ret;
}

static char *GL_Program_info_log(GLuint program_id) {
    GLint exp_len;
    glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &exp_len);
    char *ret = malloc(exp_len);
    GLint act_len;
    glGetProgramInfoLog(program_id, exp_len, &act_len, ret);
    return ret;
}

static ErrorCode GL_Program_init(GLProgram *prog,
                                 const char *vs_src,
                                 const char *fs_src) {
    ErrorCode err;
    GLint stat;

    GLuint vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, &vs_src, NULL);
    glCompileShader(vs);

    glGetShaderiv(vs, GL_COMPILE_STATUS, &stat);
    if (stat != GL_TRUE) {
        char *log = GL_Shader_info_log(vs);
        Log_debug("Failed compiling vertex shader.\n"
                  "Shader code:\n%s\n"
                  "Error log:\n%s\n",
                  vs_src,
                  log);
        free(log);
        err = ERR_GL_FAILED_COMPILING_SHADER;
        goto fail_vs;
    }

    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, &fs_src, NULL);
    glCompileShader(fs);

    glGetShaderiv(fs, GL_COMPILE_STATUS, &stat);
    if (stat != GL_TRUE) {
        char *log = GL_Shader_info_log(fs);
        Log_debug("Failed compiling vertex shader.\n"
                  "Shader code:\n%s\n"
                  "Error log:\n%s\n",
                  fs_src,
                  log);
        free(log);
        err = ERR_GL_FAILED_COMPILING_SHADER;
        goto fail_fs;
    }

    GLuint prog_id = glCreateProgram();
    glAttachShader(prog_id, vs);
    glAttachShader(prog_id, fs);
    glLinkProgram(prog_id);

    glDeleteShader(fs);
    glDeleteShader(vs);

    glGetProgramiv(prog_id, GL_LINK_STATUS, &stat);
    if (stat != GL_TRUE) {
        char *log = GL_Program_info_log(prog_id);
        Log_debug("Failed linking program.\n"
                  "Vertex shader code:\n%s\n"
                  "Fragment shader code:\n%s\n"
                  "Error log:\n%s\n",
                  vs_src,
                  fs_src,
                  log);
        free(log);
        err = ERR_GL_FAILED_LINKING_PROGRAM;
        goto fail_prog;
    }

    prog->id = prog_id;
    return ERR_NONE;

fail_prog:
    glDeleteProgram(prog_id);
    prog->id = 0;

fail_fs:
    glDeleteShader(fs);

fail_vs:
    glDeleteShader(vs);
    return err;
}

static void GL_Program_deinit(GLProgram *prog) {
    glDeleteProgram(prog->id);
}

static ErrorCode GL_Font_init(GLFont *font,
                              const char *file_path,
                              size_t pixel_size,
                              SDLRenderer *rndr) {
    ErrorCode err;

    FT_Face face;
    if (FT_New_Face(rndr->FT_ctx, file_path, 0, &face) != 0) {
        err = ERR_FT2_FAILED_FONT_LOAD;
        goto failed_face;
    }

    if (FT_Set_Pixel_Sizes(face, 0, pixel_size) != 0) {
        err = ERR_FT2_FAILED_FONT_RESIZE;
        goto failed_size;
    }

    int atlas_size = 256;
    uint8_t *atlas = malloc(atlas_size * atlas_size * sizeof(*atlas));

    int border = 1;
    int max_y = border;
    TexCoords ins_coords ={ border, border };
    for (int chr = 0; chr != 128; ++chr) {

        if (!isprint(chr))
            continue;

        if (FT_Load_Char(face, chr, FT_LOAD_RENDER) != 0) {
            err = ERR_FT2_FAILED_CHAR_LOAD;
            goto failed_load;
        }

        GLCharInfo *c_info = font->char_info + chr;
        c_info->texture_rect.coords.x = ins_coords.x;
        c_info->texture_rect.coords.y = ins_coords.y;
        c_info->texture_rect.size.w = face->glyph->bitmap.width;
        c_info->texture_rect.size.h = face->glyph->bitmap.rows;

        int reach_y = ins_coords.y + face->glyph->bitmap.rows;
        if (reach_y > max_y) max_y = reach_y;

        unsigned char *rd = face->glyph->bitmap.buffer;
        for (int y = 0; y != c_info->texture_rect.size.h; ++y) {
            uint8_t *wrt = atlas + (ins_coords.y + y) * atlas_size + ins_coords.x;
            for (int x = 0; x != c_info->texture_rect.size.w; ++x)
                *wrt++ = *rd++;
        }

        ins_coords.x += face->glyph->bitmap.width + border;
        if (ins_coords.x > atlas_size) {
            ins_coords.x = border;
            ins_coords.y = max_y + border;
        }
    }

    uint8_t *atlas_flipped = malloc(
        atlas_size * atlas_size * sizeof(*atlas_flipped));
    for (int y = 0; y != atlas_size; ++y) {
        uint8_t *rd = atlas + (atlas_size - 1 - y) * atlas_size;
        uint8_t *wrt = atlas_flipped + y * atlas_size;
        memcpy(wrt, rd, atlas_size * sizeof(*atlas));
    }

    free(atlas);

    glGenTextures(1, &font->texture);
    glBindTexture(GL_TEXTURE_2D, font->texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RED,
                 atlas_size,
                 atlas_size,
                 0,
                 GL_RED,
                 GL_UNSIGNED_BYTE,
                 atlas_flipped);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    free(atlas_flipped);

    return ERR_NONE;

failed_load:
    free(atlas);

failed_size:
    FT_Done_Face(face);

failed_face:
    return err;
}

static void GL_Font_deinit(GLFont *font) {
    glDeleteTextures(1, &font->texture);
}

static ErrorCode SDLRnd_init(SDLRenderer *rndr) {

    ErrorCode err;

    static const char *font_vs =
        "   #version 330 core                                               \n"
        "                                                                   \n"
        "   uniform mat4 uni_projection;                                    \n"
        "                                                                   \n"
        "   in vec2 attr_v_coords;                                          \n"
        "                                                                   \n"
        "   void main() {                                                   \n"
        "       gl_Position = uni_projection * vec4(attr_v_coords, 0, 1);   \n"
        "   }                                                               \n";

    static const char *font_fs =
        "   #version 330 core            \n"
        "                                \n"
        "   uniform vec4 uni_color;      \n"
        "                                \n"
        "   out vec4 out_color;          \n"
        "                                \n"
        "   void main() {                \n"
        "       out_color = uni_color;   \n"
        "   }                            \n";

    if (FT_Init_FreeType(&rndr->FT_ctx) != 0) {
        err = ERR_FT2_FAILED_INIT;
        goto failed_freetype;
    }

    if ((err = GL_Program_init(&rndr->font_program,
                               font_vs,
                               font_fs)) != ERR_NONE)
        goto failed_font_prog;

    if ((err = GL_Font_init(&rndr->default_font,
                            "./Monaco.ttf",
                            26,
                            rndr)) != ERR_NONE)
        goto failed_def_font;

    return ERR_NONE;

failed_def_font:
    GL_Program_deinit(&rndr->font_program);

failed_font_prog:
    FT_Done_FreeType(rndr->FT_ctx);

failed_freetype:
    return err;
}

static void SDLRnd_deinit(SDLRenderer *rndr) {
    GL_Program_deinit(&rndr->font_program);
    GL_Font_deinit(&rndr->default_font);
    FT_Done_FreeType(rndr->FT_ctx);
}

ErrorCode SDLRnd_create(SDL_GLContext glctx, SDLRenderer **ret) {

    SDLRenderer *rndr = malloc(sizeof(*rndr));
    rndr->glctx = glctx;

    ErrorCode err = SDLRnd_init(rndr);
    if (err != ERR_NONE) {
        free(rndr);
        *ret = NULL;
        return err;
    }

    *ret = rndr;
    return ERR_NONE;
}

void SDLRnd_destroy(SDLRenderer *rndr) {
    SDLRnd_deinit(rndr);
    free(rndr);
}
