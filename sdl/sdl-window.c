// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#include "sdl-window.h"
#include "sdl-render.h"
#include "sdl-platform.h"
#include "sdl-frame.h"

#include <GL/glew.h>
#include <GL/glu.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_video.h>

typedef struct SDLWindow {

    Window base;

    SDLWndSize wnd_size;
    SDLWndSize cell_size;

    SDL_Window *sdl_wnd;
    SDL_GLContext sdl_gl_ctx;

    SDLRenderer *sdl_rnd;

    Layout layout;

} SDLWindow;

static Layout *SDLWnd_reset_layout(Window *base) {

    SDLWindow *self = (SDLWindow *)base;
    Layout *ret = &self->layout;

    if (ret->root)
        LNd_destroy(ret->root);

    int rows = self->wnd_size.h / self->cell_size.h;
    int columns = self->wnd_size.w / self->cell_size.w;

    ret->root = LNd_create_frame((Frame *)SDLFrm_create(rows, columns));
    ret->focus = NULL;

    return ret;
}

static Key SDLWnd_await_input(Window *base) {
    (void)base;
    while (1) {
        SDL_Event event;
        if (SDL_PollEvent(&event) && event.type == SDL_KEYDOWN) {
            return event.key.keysym.sym;
        } else {
            SDL_Delay(10);
        }
    }
    return 0;
}

static void SDLWnd_repaint(struct Window *base) {
    (void)base;
}

static void SDLWnd_close(Window *base) {
    SDLWindow *self = (SDLWindow *)base;
    if (self->layout.root)
        LNd_destroy(self->layout.root);
    SDLRnd_destroy(self->sdl_rnd);
    SDL_GL_DeleteContext(self->sdl_gl_ctx);
    SDL_DestroyWindow(self->sdl_wnd);
    free(self);
}

ErrorCode SDLWnd_create(Window **wnd_base) {
    ErrorCode err;

    SDLWindow *wnd = malloc(sizeof(*wnd));
    wnd->base.reset_layout = SDLWnd_reset_layout;
    wnd->base.await_input = SDLWnd_await_input;
    wnd->base.repaint = SDLWnd_repaint;
    wnd->base.close = SDLWnd_close;

    wnd->wnd_size = (SDLWndSize){ 1920, 1080 };
    wnd->cell_size = (SDLWndSize){ 20, 30 };
    wnd->sdl_wnd = SDL_CreateWindow("K.A.N.E.",
                                    SDL_WINDOWPOS_UNDEFINED,
                                    SDL_WINDOWPOS_UNDEFINED,
                                    wnd->wnd_size.w,
                                    wnd->wnd_size.h,
                                    SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    if (!wnd->sdl_wnd) {
        err = ERR_SDL_FAILED_CREATE_WINDOW;
        goto failed_window;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);
    wnd->sdl_gl_ctx = SDL_GL_CreateContext(wnd->sdl_wnd);
    if (!wnd->sdl_gl_ctx) {
        err = ERR_SDL_FAILED_CREATE_GL_CONTEXT;
        goto failed_gl_ctx;
    }

    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
        err = ERR_SDL_FAILED_CONFIGURE_OPENGL;
        goto failed_glew;
    }

    (void)SDL_GL_SetSwapInterval(1);

    if ((err = SDLRnd_create(wnd->sdl_gl_ctx, &wnd->sdl_rnd)) != ERR_NONE)
        goto failed_sdl_gl;

    wnd->layout.root = NULL;
    wnd->layout.focus = NULL;

    *wnd_base = (Window *)wnd;
    return ERR_NONE;

failed_sdl_gl:
failed_glew:
    SDL_GL_DeleteContext(wnd->sdl_gl_ctx);

failed_gl_ctx:
    SDL_DestroyWindow(wnd->sdl_wnd);

failed_window:
    free(wnd);
    return err;
}
