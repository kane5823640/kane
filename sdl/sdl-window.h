// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SDL_WINDOW_H
#define SDL_WINDOW_H

#include "../shared.h"
#include "../platform.h"

typedef struct SDLWndCoords { int x, y; } SDLWndCoords;
typedef struct SDLWndSize { int w, h; } SDLWndSize;

ErrorCode SDLWnd_create(Window **wnd_base);

#endif
