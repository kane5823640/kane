// Copyright (C) 2020 Krzysztof Stachowiak
//
// This file is part of the Language Programming Language compiler.
//
// Language Programming Language is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Language Programming Language compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with program.  If not, see <http://www.gnu.org/licenses/>.

#include "sdl-platform.h"
#include "sdl-window.h"

#include <SDL2/SDL.h>

#include <stdlib.h>

typedef struct SDLPlatform {
    Platform base;
} SDLPlatform;

static ErrorCode SDLPfrm_open_window(Platform *base, Window **wnd_base) {
    (void)base;
    return SDLWnd_create(wnd_base);
}

static void SDLPfrm_destroy(Platform *base) {
    SDL_Quit();
    free(base);
}

ErrorCode SDLPfrm_init(Platform **ret) {

    ErrorCode err;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        err = ERR_SDL_FAILED_SETUP_BASICS;
        goto failed_basics;
    }

    SDLPlatform *self = malloc(sizeof(*self));

    Platform *base = (Platform *)self;
    base->open_window = SDLPfrm_open_window;
    base->destroy = SDLPfrm_destroy;

    *ret = base;
    return ERR_NONE;

failed_basics:
    *ret = NULL;
    return err;
}
